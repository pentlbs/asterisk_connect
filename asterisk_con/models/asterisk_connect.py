# -*- coding: utf-8 -*-
import time
import math

from odoo.osv import expression
from odoo.tools.float_utils import float_round as round
from odoo.exceptions import UserError, ValidationError
from odoo import api, fields, models, _

class AsteriskConnect(models.Model):
    _name = 'asterisk.connect'
    
    name = fields.Char(string=u'Тохиргооны нэр')
    ast_ip_address = fields.Char(string=u'Asterisk IP хаяг')
    ast_username = fields.Char(string=u'Asterisk нэвтрэх нэр')
    ast_password = fields.Char(string=u'Asterisk нууц үг')
    db_name = fields.Char(string=u'Датабаз нэр')
    db_username = fields.Char(string=u'Датабаз хэрэглэгч нэр')
    db_password = fields.Char(string=u'Датабаз нууц үг')
    
    
    
class AsteriskPayment(models.Model):
    _name = 'asterisk.payment'
    
    name = fields.Datetime(string=u'Ярьсан огноо')
    total_minut = fields.Float(string=u'Ярьсан минут')
    price = fields.Float(string=u'Үнэ')
    total_price = fields.Float(string=u'Нийт үнэ')
    
    
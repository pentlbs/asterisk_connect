# -*- coding: utf-8 -*-

{
    'name': 'Asterisk CONNECT',
    'category': 'Sale',
    'version': '13.0',
    'author': 'Ubuntu ERKA',
    'company': 'UBUNTU',
    'maintainer': 'UBUNTU',
    'website': '',
    'summary': '',
    'images': [],
    'description': "Odoo ASTERISK MODULE",
    'depends': [
        'base',
    ],
    'data': [
        'security/security_view.xml',
        'security/ir.model.access.csv',
        'views/asterisk_connect_view.xml',
        'views/asterisk_payment_view.xml',
        'views/menu_view.xml',

    ],
    'license': 'AGPL-3',
}
